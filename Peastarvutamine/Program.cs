﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peastarvutamine
{
    class Program
    {
        static void Main(string[] args)
        {
            // tavaline for tsükkel
            //for (int i = 0; i < 10; i++)
            //  {
            //      Console.WriteLine($"Teen seda asja {i}-{(i == 1 || i == 2 ? "st" : "ndat")} korda");

            //juhuslik number



            int punkte = 50;
            for (int i = 0; i < 10; i++)
            {
                Random r = new Random();
                int a = r.Next(10);
                int b = r.Next(10);
                int kontroll = 0;
                string tehe = " ";
               

                switch (r.Next(4))
                {
                    case 0:
                        tehe = "summa";
                        kontroll = a + b;
                        break;
                    case 1:
                        tehe = "vahe";
                        kontroll = a - b;
                        break;
                    case 2:
                        tehe = "korrutis";
                        kontroll = a * b;
                        break;
                    case 3:
                        tehe = "jagatis";
                        kontroll = a / b;
                        break;
                }
                Console.WriteLine($"leia arvude {a} ja {b} {tehe}: ");
                for (int j = 0; j < 5; j++)
                {
                    if (int.Parse(Console.ReadLine()) == kontroll) break;
                    Console.Write("vale vastus");
                    if (j == 4) Console.WriteLine();
                    else Console.WriteLine("Proovi uuesti");
                    punkte--;
                }
            }
                Console.WriteLine(
                    punkte == 50  ?  "suurepärane - kõik õiged vastused":
                    punkte > 40  ?  "üsna tubli tulemus" :
                    punkte > 30  ?  "päris hea tulemus" :
                    punkte > 10  ?  "kehvake, harjuta veel":
                                    "mine algkooli tagasi"
              );
                    
        
        }
    }
}
                  

            // küsimus, mis on arvude {x} {y} summa/vahe/korrutis/jagatis


            //Loe  vastus: Kui vastus on vale, siis küsi uuesti(ütle, et oli vale)


            //kokku esita 10 küsimust


            //iga vale vastus annab miinuspunkti


            /*lõpuks ütle hinne
            0...10  - mine algkooli tagasi
            10...30 - harjuta veel
            30...40 - päris hea
            40...50 - tubli
            50      - suurepäeane
            */
