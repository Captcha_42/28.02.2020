﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Pordipaeva_ylesanne_1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Loe protokoll masinasse
            string filename = @"..\..\100m-jooksu-tulemused.txt";

            var read = File.ReadAllLines(filename);

            string[] nimed = new string[read.Length - 1];
            double[] kiirused = new double[read.Length - 1];
            int kiireim = 0;
            
            for (int i = 0; i < nimed.Length; i++)
            {
                string[] tykid = read[i + 1].Replace(", ", ",").Split(',');
                nimed[i] = tykid[0];
                kiirused[i] = double.Parse(tykid[1]) / double.Parse(tykid[2]);
                if (kiirused[i] > kiirused[kiireim]) kiireim = i;
            }
            Console.WriteLine($"\nkõikse kiirem on {nimed[kiireim]} oma kiirusega {kiirused[kiireim]:F2}");
            Console.WriteLine($"\njooksuajad on: {kiirused[i]:F2}");
        }
    }
}
